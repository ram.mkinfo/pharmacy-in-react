import React from 'react'
import Content from '../Content/Content'
import NavBar from '../NavBar/NavBar'
import './Home.css'
export default function Home() {
  return (
    <div className='Home'>
    <NavBar/>
    <Content/>
    </div>
  )
}
